from django.shortcuts import render

# Create your views here.

from django.views.generic import ListView
from prshl.models import Work


class IndexView(ListView):
    model = Work
    context_object_name = 'all_the_work'
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context
