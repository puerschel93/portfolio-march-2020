(function(){
    "use strict";
    window.addEventListener('DOMContentLoaded', init, false);
    function init(){
        document.body.style.overflow = 'hidden'
        setTimeout(hideLoader, 3000)

    }

    function hideLoader(){
        document.body.style.overflow = 'visible'
        let elem = document.getElementById('loader')
        elem.parentNode.removeChild(elem)
    }
})();
