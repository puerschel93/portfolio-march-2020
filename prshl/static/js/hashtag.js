(function(){
    "use strict";

    window.addEventListener('DOMContentLoaded', init, false)

    function init(){
        let hashtags = document.getElementsByClassName('caption__tags')
        let arrayHashtags = Array.prototype.slice.call(hashtags);

        arrayHashtags.forEach(element => {
            let tags = element.innerHTML;
            let empt = tags.replace(/ /g, '').split(',')
            let full = ''
            empt.forEach(element => {
                full = full + '#' + element + ' '
            })
            element.innerHTML = '' + full
        })
    }
})();