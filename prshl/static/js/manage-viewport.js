(function(){
    "use strict";
    if(location.hash != ''){
        location.replace("./");
    }
    window.addEventListener('load', init, false);
    function init(){
        let navArray = document.querySelectorAll('main')
        navArray = Array.from(navArray)
        let pbar = document.getElementById('pg-bar')
        checkElements(navArray);
        scalePbar(pbar)
        document.addEventListener('scroll', function(){
            checkElements(navArray);
            scalePbar(pbar);
        } )
    }

    function scalePbar(bar){
        let bodyHeight = document.body.getBoundingClientRect().height
        let progress = (((window.scrollY) / (bodyHeight - window.innerHeight)) * 100)
        bar.style.width = progress + 'vw'
    }

    function checkElements(arr){
        let windowHalf = window.innerHeight / 4
        let header = document.getElementById('header')
        if(window.scrollY > windowHalf){
            if(!header.classList.contains('header--scrolled')){
                header.classList.add('header--scrolled')
            }
        } else {
            if(header.classList.contains('header--scrolled')){
                header.classList.remove('header--scrolled')
            }
        }

        arr.forEach(function(element) {
            let clitop = element.getBoundingClientRect().top.toFixed(0);
            let cliheight = element.getBoundingClientRect().height.toFixed(0);
            if(clitop <= windowHalf && clitop > -cliheight){
                let navEl = Array.from(document.querySelectorAll('[name="' + element.id + '"]'));
                if(!navEl[0].classList.contains('navigation__element--active')){
                    navEl[0].classList.add('navigation__element--active')
                }
            } else if(clitop > 0 || clitop <= -cliheight){
                let navEl = Array.from(document.querySelectorAll('[name="' + element.id + '"]'));
                if(navEl[0].classList.contains('navigation__element--active')){
                    navEl[0].classList.remove('navigation__element--active')
                }
            }
        });
    }
})();