(function(){
    "use strict";

    window.addEventListener('load', init, false)
    function init(){
        let menuIcon = document.getElementById('menu-unfolder')
        let navigation = document.getElementById('navigation')
        menuIcon.addEventListener('click', function(){
            navigation.classList.toggle('header__navigation--unfolded')
            if(navigation.classList.contains('header__navigation--unfolded')){
                menuIcon.name = "close-sharp"
            } else {
                menuIcon.name = "menu-sharp"
            }
        },false)
    }
})();