from django.db import models

# Create your models here.

from datetime import date
from django.db import models


class Work(models.Model):

    WORK_TYPE = [
        ('prototype', 'Prototype'),
        ('mockup', 'Mockup'),
        ('redesign', 'Redesign'),
        ('branding', 'Branding'),
        ('illu', 'Illustration'),
        ('java', 'Java'),
        ('python', 'Python'),
        ('django', 'Django'),
        ('concept', 'Concept'),
        ('sys', 'Design System'),

    ]

    title = models.CharField(max_length=50, blank=False)
    description = models.TextField(max_length=240, blank=True)
    work_type = models.CharField(max_length=10, choices=WORK_TYPE, blank=False)
    samplePicture = models.CharField(max_length=100, blank=False)
    link = models.URLField(blank="false")
    tags = models.CharField(max_length=100, blank=True)
    modified_date = models.DateTimeField(auto_now=True)
    timeAdded = models.DateField(blank=False,
                                 default=date.today)

    class Meta:
        ordering = ['-modified_date']

    def type_lower(self):
        return self.get_work_type_display().lower()

    def __str__(self):
        return self.title + '\t/\t' + self.timeAdded.strftime('%d.%m.%Y')

    def __repr__(self):
        return self.title + '\t/\t' + self.timeAdded.strftime('%d.%m.%Y')